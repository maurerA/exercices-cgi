package com.cgi.tests;

import com.cgi.topic1.Develloper;
import com.cgi.topic1.Manager;
import com.cgi.topic1.Skills;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class DevelloperTest extends TestCase {

 public Manager manager = new Manager() ;
 public Develloper dev = new Develloper() ;
 public ArrayList<Skills> listSkill = new ArrayList<>();

    public DevelloperTest() {
    }

    @Before
    public void TestDevelopper(){
        System.err.println("Tests sur l'objet DEVELOPPEUR'\n----------------------------------------------------------");
    }

    @Test
    public void testGetManager() {
        dev.setManager(manager);
        manager.setNom("hello");
        assertEquals(manager,dev.getManager());
    }
    @Test
    public void testSetManager()  {
        dev.setManager(manager);
        assertEquals(manager, dev.getManager());
    }
    @Test
    public void testGetSkills() {
        listSkill.add(Skills.JAVA);
        listSkill.add(Skills.REACT);
        dev.setSkills(listSkill);
        assertEquals(listSkill,dev.getSkills());
    }
    @Test
    public void testSetSkills() {
        listSkill.add(Skills.DELIVERY);
        dev.setSkills(listSkill);
        assertTrue(dev.getSkills().equals(listSkill));
    }
    @Test
    public void testAddSkills() {
        dev.addSkills(Skills.ANGULAR);
        assertTrue("l'arrayList contient Angular",dev.getSkills().contains(Skills.ANGULAR));

    }

    public void testTestToString() {

    }
}