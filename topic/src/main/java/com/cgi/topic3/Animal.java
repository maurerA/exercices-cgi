package com.cgi.topic3;

public class Animal {
    private String nom;
    private int age ;
    private Sexe sexe;
    public static final int nbrDePattes = 4;
    public static final String son = "whouf!!!!;miaou!!!!!";



    public enum Sexe{
        MALE,FEMELLE
    }

    public Animal(String nom, int age, Sexe sexe) {
        this.nom = nom.toUpperCase();
        this.age = age;
        this.sexe = sexe;


    }

    public String son(){
        return "je suis un animal ";
    }

    @Override
    public String toString() {

        return
                nom +
                        "-" + age +
                        "-" + sexe +
                        "-"+nbrDePattes*age+""+ nom.charAt(0)+"-"+son()
                ;
    }
}
