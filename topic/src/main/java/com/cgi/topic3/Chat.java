package com.cgi.topic3;

public class Chat extends Animal{
    public Chat(String nom, int age, Sexe sexe) {
        super(nom, age, sexe);
    }

    @Override
    public String son() {
        String [] sons = son.split(";");
        return sons[1];
    }

    @Override
    public String toString() {

        return "Chat- " + super.toString()+"\n";
    }
}

