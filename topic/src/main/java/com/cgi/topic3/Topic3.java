package com.cgi.topic3;

import com.cgi.App;
import com.cgi.topic2.Topic2;
import com.cgi.topic2.TraitementException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

public class Topic3 {public static void main3() throws ParseException, TraitementException, TraitementException {
    String choix ;
    Scanner scanner=new Scanner(System.in);
    boolean flag=true;
    ArrayList<Animal> listAnimaux=new ArrayList<>();
    Chien chien = new Chien("lala",22, Animal.Sexe.FEMELLE);
    Chat chat = new Chat("lili",3, Animal.Sexe.MALE);
    listAnimaux.add(chien);
    listAnimaux.add(chat);

    do {
        System.out.println("Par default deux animaux sont créés.");
        System.out.println("[1]- Pour créer un chien.");
        System.out.println("[2]- Pour créer un chat.");
        System.out.println("[3]- Afficher dans la console, pour tous les animaux créés : « Nom » – « Age » – « Sexe » - Age X patte + la première lettre du sexe");
        System.out.println("Mainteant, il faut choisir l'exercice 1,2 ou 3 (0 pour sortir):");
        choix= scanner.next();
        while(App.gestionDesErreurs(choix)==false){
            choix = scanner.next();
        }
        switch (Integer.parseInt(choix)){
            case 1 :

            case 2 :
                listAnimaux = CreatChienOrChat(Integer.parseInt(choix),listAnimaux);
                break;
            case 3:
                System.out.println("Afficher les animaux");
                System.err.println(listAnimaux.toString());
                break;
            case 0:
                System.out.println("Cette partie de l'exercice est terminée!!!!");
                flag=false;
                break;
            default:
                System.out.println("une erreur s'est produite !!!Désole pour le dérangement.");
                break;
        }

    }while (flag==true);
    App main = new App();
    String [] args = null;
    main.main(args);

}

    public static ArrayList<Animal> CreatChienOrChat(int number,ArrayList<Animal>listAnimaux) {

        Animal.Sexe genre = null;

        boolean flag = true;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Veuillez saisir le nom de l'animal.");
        String nomAnimal = scanner.next();
        System.out.println("Veuillez saisir l'âge de l'animal.");
        String ageAnimal = scanner.next();
        while (Topic2.gestionExceptionsNombreElements(ageAnimal)==false) {
            ageAnimal = scanner.next();
        }
        System.out.println("Veuillez saisir le sexe de l'animal avec 'M' pour male et 'F' pour femelle.");
        String sexe = scanner.next();
        while (flag == true) {
            if (sexe.equalsIgnoreCase("M")) {
                genre = Animal.Sexe.MALE;
                flag = false;
            } else if (sexe.equalsIgnoreCase("F")) {
                genre = Animal.Sexe.FEMELLE;
                flag = false;
            }else{
                System.out.println("Veuillez saisir le sexe de l'animal avec 'M' pour male et 'F' pour femelle.");
                sexe = scanner.next();
            }

        }
        if (number == 1) {
            Chien chien = new Chien(nomAnimal, Integer.parseInt(ageAnimal), genre);
            listAnimaux.add(chien);
            System.out.println("Vous avez créé : "+chien.toString());
            return listAnimaux;
        } else if (number == 2) {
            Chat chat = new Chat(nomAnimal, Integer.parseInt(ageAnimal), genre);
            listAnimaux.add(chat);
            System.out.println("Vous avez créé : "+chat.toString());
            return listAnimaux ;
        } else {
            System.out.println("Une erreur s'est produite");
        }
        return null;

    }
}
