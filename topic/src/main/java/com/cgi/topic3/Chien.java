package com.cgi.topic3;

public class Chien extends Animal{
    public Chien(String nom, int age, Sexe sexe) {
        super(nom, age, sexe);
    }

    @Override
    public String son() {
        String [] sons = son.split(";");
        return sons[0];
    }

    @Override
    public String toString() {

        return "Chien- " + super.toString()+"\n";
    }
}
