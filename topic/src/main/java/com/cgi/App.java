package com.cgi;

import com.cgi.topic1.Topic1;

import com.cgi.topic2.Topic2;
import com.cgi.topic2.TraitementException;
import com.cgi.topic3.Topic3;

import java.text.ParseException;
import java.util.Scanner;


public class App 
{
    public static void main(String[] args) throws ParseException, TraitementException {
        Scanner scanner = new Scanner(System.in);
        String nbrExo;
        boolean flag = true;

        /*Menu general des exos*/
        do{

            System.out.println("Vous avez la possibilité d'accèder aux trois exercices en tapant 1,2 ou 3 (0 pour sortir).\n" +
                    "Vous aurez pour chaque exercice un petit résumé. Bon amusement!!!\n"+"Mainteant, il faut saisir 1,2 ou 3 pour choisir l'exercice désiré(0 pour sortir):" );

            nbrExo = scanner.next();
            /*Controle du nombre rentre par l'utilsiateur*/
            while (gestionDesErreurs(nbrExo)==false){
                nbrExo = scanner.next();
            }

            switch (Integer.parseInt(nbrExo)){

                case 1:
                    System.out.println("Context : Il y a 4 développeurs déjà créés :\n"+ "" +
                            "Pierre MARCHAND,\n"+"Paul DURANT,\n" + "Jacques ADIT \n" +"et JuliePACOT\n" +
                            "il y a également 2 managers créés d'office :\n"+"Sam Debit et Raumane DUMONT.\n" +
                            "Sam est le manager de Jacques et Julie.\n"+"Raumane est la manager de JAcques et Julie.\n" +
                            "Chaque développeur à au moins 2 compétences.\n" +
                            "L'appli pourra lister le nom, prenom et l age des menbres qui ont une compétence et le nom et prénom du manager à contacter.\n" +
                            "On pourra lister les personnes qui mesurent plus une taille à définir. ");
                    /*topic 1*/
                    Topic1 topic1 = new Topic1();
                    topic1.main1();
                    break;

                case 2:
                    System.out.println("Context : Dans un tableau Array, parcourir le tableau et retourner l’index d’un certain nombre." +
                            " Il faut également gérer les exceptions ");
                    /*topic 2*/
                    Topic2 topic2 = new Topic2();
                    topic2.main2();
                    break;

                case 3:
                    System.out.println("Context : 3 classes (Animal(mère), Chien(fille), Chat(fille))\n" +
                            "Attribut : Age, Nom, Sexe\n" +
                            "Constante : Nb Patte et Son\n" +
                            "Afficher dans la console, pour tous les animaux créés : « Nom » – « Age » – « Sexe » - Age X patte + la première lettre du nom – « son » \n" +
                            "Ex : pour le chien (« toto » ; « 15 » ; »Male ») : TOTO – 15 – Male – 60M – wouaf\n");
                    /*Topic 3 les animaux*/
                    Topic3 topic3 = new Topic3();
                    topic3.main3();
                    break;

                case 0 :
                    System.out.println("Merci de votre visite à bientôt!!!!");
                    flag=false;
                    break;

                default :
                    System.out.println("une erreur s'est produite !!!Désole pour le dérangement.");
                    break;
            }

        }while(flag==false);

    }
    /*
    gestion des erreurs sur la saisie de l'utilsateur
    un string en argument
     */
    public static boolean gestionDesErreurs(String number)  {
        boolean reponse = false;
        try {
            if (!number.isEmpty()) {
                int ok = Integer.parseInt(number);
                if(ok == 1 || ok ==2 || ok==3 || ok==0) {
                    return reponse=true;
                }}
        } catch (Exception traitementException) {
            traitementException.printStackTrace();
            System.out.println("Votre saisie est incorrecte veuillez recommencer :");
            return reponse;
        }
        System.out.println("Votre saisie est incorrecte veuillez recommencer :");
        System.out.println("Vous avez la possibilité d'accèder aux trois exercices en tapant 1,2 ou 3 (0 pour sortir).\n" +
                "Vous aurez pour chaque exercice un petit résumé. Bon amusement!!!\n"+"Mainteant, il faut saisir 1,2 ou 3 pour choisir l'exercice désiré(0 pour sortir):" );
        return reponse;
    }
}

