package com.cgi.topic1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;


public abstract class Personne {

    public enum Genre {
        M,F
    }
    private Genre sexe;
    private String nom;
    private String prenom;
    private Date anniversaire = new Date();
    private int taille;

    public Personne(Genre sexe, String nom, String prenom, String anniversaire, int taille) throws ParseException {

        this.sexe = sexe;
        this.nom = nom.toUpperCase();
        this.prenom = prenom;

        this.anniversaire = new SimpleDateFormat("dd/MM/yyyy").parse(anniversaire);
        this.taille = taille;
    }

    public Personne() {
    }

    public int getTaille() {
        return taille;
    }

    public void setTaille(int taille) {
        this.taille = taille;
    }

    public Genre getSexe() {
        return sexe;
    }

    public void setSexe(Genre sexe) {
        this.sexe = sexe;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getAnniversaire() {
        return anniversaire;
    }

    public void setAnniversaire(Date anniversaire) {
        this.anniversaire = anniversaire;
    }

    @Override
    public String toString() {
        return "Personne{" +
                "sexe=" + sexe +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", anniversaire=" + anniversaire +
                ", taille=" + taille +
                '}';
    }
    public int calculateAge(
    ) {
        LocalDate birthDate = Instant.ofEpochMilli(anniversaire.getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
        LocalDate currentDate = LocalDate.now();
        return Period.between(birthDate, currentDate).getYears();
    }

}
