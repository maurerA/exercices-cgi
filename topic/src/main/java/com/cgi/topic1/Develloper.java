package com.cgi.topic1;

import java.text.ParseException;
import java.util.ArrayList;

public class Develloper extends Personne{
    private Manager manager;
    private ArrayList skills = new ArrayList<Skills>();

    public Develloper(Genre sexe, String nom, String prenom, String anniversaire,int taille) throws ParseException {
        super(sexe, nom, prenom, anniversaire,taille);

    }

    public Develloper() {
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public ArrayList getSkills() {
        return skills;
    }

    public void setSkills(ArrayList skills) {
        this.skills = skills;
    }
    public void addSkills(Skills skills){
        this.skills.add(skills);
    }



    @Override
    public String toString() {
        return "Le développeur "+super.getPrenom()+" "+super.getNom()+" âge de "+super.calculateAge()+" a pour manager "+this.getManager().getPrenom()+" "+this.getManager().getNom();
    }


}
