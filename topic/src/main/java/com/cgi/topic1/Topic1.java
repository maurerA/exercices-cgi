package com.cgi.topic1;

import com.cgi.App;
import com.cgi.topic2.TraitementException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Topic1 {
    public static void main1() throws ParseException, TraitementException {

        ArrayList<Manager> listManager=createAutoManager();
        ArrayList<Develloper>listDev=createAutoDev();
        ArrayList<Personne>listePersonnel = new ArrayList<>();
        listePersonnel.addAll(listManager);
        listePersonnel.addAll(listDev);
        boolean flag = true;
        String choix;
        /*Recuperation du choix de l utilisateur*/
        Scanner scanner = new Scanner(System.in);
        /*
        Boucle do while pour la presentation des actions des utilisateurs + validation des entrées
         */
        do{
            System.out.println("[1]- Pour créer un manager et l'associer à un developper");
            System.out.println("[2]- Pour créer un developper et l'associer à un manager");
            System.out.println("[3]- Choisir une compétence et afficher le nom, prénom et âge des développeurs et le nom, le prénom du manager associé");
            System.out.println("[4]- Lister toutes les personnes par rapport à une taille en cm");
            System.out.println("[5]- Affiche la liste du personnel, la liste des managers et des développeurs");
            System.out.println("Maintenant, il faut choisir l'exercice 1,2,3,4 et 5 (0 pour sortir):");
            choix = scanner.next();
            while (gestionDesErreurs(choix)==false){
                choix = scanner.next();
            }

            switch (Integer.parseInt(choix)){

                case 1:
                    boolean validManager = false;
                    System.out.println("Vous allez creer un manager");
                    Manager manager = creationManager();
                    listManager.add(manager);
                    System.out.println("Veuillez saisir un developpeur pour l'associer à votre manager dans la liste suivante :");
                    for (Develloper dev: listDev
                    ) {
                        System.out.println("Le déveloopeur : "+dev.getNom()+" est associé au manager "+dev.getManager().getNom());
                    }

                    while(validManager==false){
                        System.out.println("A quel nom de devellopeur voulez vous associer votre manager en sachant qu'un developpeur ne peut avoir qu'un seul manager :");
                        String nomDev = scanner.next();
                        for (Develloper dev: listDev
                        ) {
                            if(nomDev.equalsIgnoreCase(dev.getNom())){
                                addManagerToDev(dev,manager);
                                System.out.println("Votre modification a été pris en compte. Le développeur "+dev.getNom()+" a pour manager "+dev.getManager().getNom()+" "+dev.getManager().getPrenom()+" maintenant");
                                validManager=true;
                                break;
                            }
                        }
                        listePersonnel.clear();
                        listePersonnel.addAll(listManager);
                        listePersonnel.addAll(listDev);
                    }
                    break;
                case 2:
                    boolean skillOne = false;
                    boolean skillTwoo = false;
                    String firstSkill=null;
                    Skills skill1 = null;
                    Skills skill2 = null;
                    String secondSkill=null;
                    boolean verif = false;
                    System.out.println("Vous allez creer un développeur ");
                    Develloper develloper = creationDev();
                    System.out.println("Veuillez ajouter deux compétence à votre développeur parmi la liste suivante :\n" + Skills.JAVA.toString() +
                            " \n" + Skills.ANGULAR.toString() + " \n" + Skills.GCP.toString() + " \n" + Skills.DELIVERY.toString() + " \n" + Skills.SELENIUM.toString() +
                            " \n" + Skills.REACT.toString() + " \n" + Skills.JENKINS.toString() + " \n" + Skills.PHP.toString());

                    while(skillOne==false&&skillTwoo==false) {
                        System.out.println("Veuillez ajouter un premier composant :");
                        firstSkill = scanner.next();
                        System.out.println("Veuillez ajouter un deuxieme composant :");
                        secondSkill = scanner.next();
                        ArrayList<Skills>listSkill=listSkill();
                        for (Skills skill :
                                listSkill) {
                            if (firstSkill.equalsIgnoreCase(secondSkill)||firstSkill.isEmpty()||secondSkill.isEmpty()||firstSkill==null||secondSkill==null) {
                                System.out.println("Vos deux champs sont égaux ou vide, Veuillez recommencer.");
                                break;
                            }else if(firstSkill.equalsIgnoreCase(skill.toString())){
                                skill1=skill;
                                skillOne=true;
                            }else if (secondSkill.equalsIgnoreCase(skill.toString())){
                                skill2=skill;
                                skillTwoo=true;
                            }
                        }
                    }
                    develloper.addSkills(skill1);
                    develloper.addSkills(skill2);
                    System.out.println("Vous avez ajoute au developpeur deux compétences :"+develloper.getSkills().toString());
                    System.out.println("Vous pouvez ajouter un manager à votre developpeur dans la liste ci-dessous : ");


                    for (Manager m :
                            listManager) {
                        System.out.println("Le manager : "+m.getNom()+" "+m.getPrenom());
                    }
                    while(verif==false){
                        System.out.println("Veuillez saisir le nom du manager : ");
                        String nomManager = scanner.next();

                        for (Manager m :
                                listManager) {
                            if (!nomManager.isEmpty() && nomManager.equalsIgnoreCase(m.getNom())) {
                                addManagerToDev(develloper,m);
                                listDev.add(develloper);
                                System.out.println("Vous avez cree un nouveau developpeur");
                                verif = true;
                            }
                        }
                        System.out.println(listDev.toString());
                    }
                    listePersonnel.clear();
                    listePersonnel.addAll(listManager);
                    listePersonnel.addAll(listDev);
                    break;

                case 3:
                    System.out.println("Lister les noms et prénoms et l'âge des menbres qui ont la compétence java et le nom et prénom du manager à contacter");
                    System.out.println("Choisisez une compétences parmi la liste suivante : ");

                    List<Develloper> listTrie =  listDev.stream().filter((dev)-> dev.getSkills().contains(Skills.JAVA)).collect(Collectors.toList());
                    listeDevJava(listTrie);
                    break;

                case 4:
                    System.out.println("Lister les noms et prénoms et l'âge des menbres qui sont strictement supérieur à 160");
                    listPersonneSup160(listePersonnel);
                    break;
                case 5:
                    System.out.println("Affiche la liste du personnel, la liste des managers et des développeurs");

                    System.out.println("la liste des dev :");
                    for (Develloper d :
                            listDev) {
                        System.out.println("Le développeur : "+d.getNom()+" "+d.getPrenom());
                    }
                    System.out.println(" ---------------------------------------------------- ");
                    for (Manager m :
                            listManager){
                        System.out.println("Le manager : "+m.getNom()+" "+m.getPrenom());
                    }
                    System.out.println(" ---------------------------------------------------- ");
                    for (Personne p :
                            listePersonnel) {
                        System.out.println("Le personnel : "+p.getNom()+" "+p.getPrenom());
                    }
                    break;


                case 0 :
                    System.out.println("Cette partie de l'exercice est terminée!!!!");
                    flag=false;
                    break;
                default:
                    System.out.println("Une erreur s'est produite !!!Désole pour le dérangement.");
                    break;

            }

        }while(flag==true);
        App main = new App();
        String [] args = null;
        main.main(args);
    }
    /*
    methode de creation d un dev avec celui ci ds le retour.
     */
    private static Develloper creationDev() throws ParseException {
        boolean result = false;
        boolean flag=false;

        String regexDate = "^\\d{2}/\\d{2}/\\d{4}$";
        String date = null;
        Personne.Genre genreHumain = Personne.Genre.F;
        Scanner clavier = new Scanner(System.in);


        while(flag == false){
            System.out.println("Choisir son genre M/F (Tapez M ou F) : ");
            String genre = clavier.next();
            if(genre.equalsIgnoreCase("m")){
                genreHumain = Personne.Genre.M;
                flag = true;
            }else if(genre.equalsIgnoreCase("f")){
                genreHumain = Personne.Genre.F;
                flag = true;
            }
        }
        System.out.println("Choisir son nom : ");
        String nom = clavier.next();
        System.out.println("Choisir son prénom : ");
        String prenom = clavier.next();

        while(result==false) {
            System.out.println("Sa date d'anniversaire sous la forme jj/MM/AAAA :");
            date = clavier.next();
            result = date.matches(regexDate);
        }
        System.out.println("Veuillez saisir la taille en centimètre : ");
        String taille = clavier.next();
        while(gestionDesErreurs(taille)==false){
            taille = clavier.next();
        }

        Develloper develloper = new Develloper(genreHumain,nom,prenom,date,Integer.parseInt(taille));
        return develloper;

    }

    /*Methode permettant de creer un manager
    retourne l'objet manager construit*/
    public static Manager creationManager() throws ParseException {
        boolean result = false;
        boolean flag=false;

        String regexDate = "^\\d{2}/\\d{2}/\\d{4}$";
        String date = null;
        Personne.Genre genreHumain = Personne.Genre.F;
        Scanner clavier = new Scanner(System.in);

        while(flag == false){
            System.out.println("Choisir son genre M/F (Tapez M ou F) : ");
            String genre = clavier.next();
            if(genre.equalsIgnoreCase("m")){
                genreHumain = Personne.Genre.M;
                flag = true;
            }else if(genre.equalsIgnoreCase("f")){
                genreHumain = Personne.Genre.F;
                flag = true;
            }
        }
        System.out.println("Choisir son nom : ");
        String nom = clavier.next();
        System.out.println("Choisir son prénom : ");
        String prenom = clavier.next();

        while(result==false) {
            System.out.println("Sa date d'anniversaire sous la forme jj/MM/AAAA :");
            date = clavier.next();
            result = date.matches(regexDate);
        }
        System.out.println("Veuillez saisir la taille en centimètre : ");
        String taille = clavier.next();
        while(gestionDesErreurs(taille)==false){
            taille = clavier.next();
        }

        Manager manager = new Manager(genreHumain,nom,prenom,date,Integer.parseInt(taille));
        return manager;

    }
    /* Methode permettant de lister les dev ayant la compétence java*/
    public static void listeDevJava(List<Develloper> listTrie){
        System.out.println("les personnes ayant la compétences JAVA sont : ");
        for (Develloper dev :
                listTrie) {
            System.out.println(dev.toString());
        }
    }
    /*Methode permettant d'associer un manager à un dev et un dev à un manager*/
    public static void addManagerToDev(Develloper dev, Manager manager) throws ParseException {
        dev.setManager(manager);
        manager.addMenber(dev);
    }
    /*Methode permettant d'ajouter deux compétences à un dev*/
    public static void addSkillsToDev(Develloper dev, Skills skills,Skills skill){
        dev.addSkills(skills);
        dev.addSkills(skill);
    }
    /*Methode permettant de lister l'ensemble des personnes par rapport à une taille */
    public static void listPersonneSup160 (ArrayList<Personne> listePersonne){
        List<Personne> listePer = listePersonne.stream().filter((p)-> p.getTaille()>160).collect(Collectors.toList());
        for (Personne p:listePer
        ) {
            System.err.println(p.getPrenom()+" "+p.getNom()+" mesure strictement plus de 160 cm");
        };
    }
    /*
    Methode d'instanciation auto des 4 dev
    retourne une liste des devellopeurs autogeneré
    */
    public static ArrayList<Develloper> createAutoDev() throws ParseException {
        /* Création de 4 dev */
        Develloper pierreMarchand = new Develloper(Personne.Genre.M,"Marchand","pierre","14/02/1989",180);
        Develloper paulDurant = new Develloper(Personne.Genre.M,"durant","paul","18/07/1991",178 );
        Develloper jacquesAdit = new Develloper(Personne.Genre.M,"adit","jacques","24/12/1980",160);
        Develloper juliePacot = new Develloper(Personne.Genre.F,"pacot","julie","09/08/1976",155);

        /*ajout du manager au dev et associe le manager au dev*/
        ArrayList<Manager>listMAnager = createAutoManager();
        for (Manager m: listMAnager
        ) {
            if(m.getNom().equalsIgnoreCase("Debif")){
                addManagerToDev(pierreMarchand,m);
                addManagerToDev(paulDurant,m);
            }else if(m.getNom().equalsIgnoreCase("Dumont")){
                addManagerToDev(jacquesAdit,m);
                addManagerToDev(juliePacot,m);
            }
        }

        /*Ajout de compétences aux developpeur*/
        addSkillsToDev(pierreMarchand,Skills.ANGULAR,Skills.JAVA);
        addSkillsToDev(paulDurant,Skills.ANGULAR,Skills.REACT);
        addSkillsToDev(jacquesAdit,Skills.DELIVERY,Skills.GCP);
        addSkillsToDev(juliePacot,Skills.JAVA,Skills.SELENIUM);

        /*liste de dev*/
        ArrayList<Develloper>listeDev = new ArrayList<>();
        listeDev.add(pierreMarchand);
        listeDev.add(paulDurant);
        listeDev.add(jacquesAdit);
        listeDev.add(juliePacot);


        return listeDev;
    }
    /* Methode d'instanciation auto des 2 managers
     retoune la liste des manager auto genere*/
    public static ArrayList<Manager> createAutoManager() throws ParseException {
        /*Creation des managers*/
        Manager samDebif = new Manager(Personne.Genre.F,"DEBIF","sam","07/09/1972",168);
        Manager raumaneDumont = new Manager(Personne.Genre.F,"dumont","raumane","04/04/1983",154);
        ArrayList<Manager>listManager = new ArrayList<>();
        listManager.add(samDebif);
        listManager.add(raumaneDumont);
        return listManager;
    }
    /*
    methode de validation des entrees utilisateurs
     */
    public static boolean gestionDesErreurs(String number) {
        boolean reponse = false;
        try {
            if (!number.isEmpty()) {
                int ok = Integer.parseInt(number);
                reponse = true;
                return reponse;
            }
        } catch (Exception traitementException) {
            traitementException.printStackTrace();
            System.out.println("Votre saisie est incorrecte veuillez recommencer :");
            return reponse;
        }

        return reponse;
    }
    /*
    methode pour faciliter le traitement des shills
    retour de la liste des skills
     */
    public static ArrayList<Skills> listSkill(){
        ArrayList<Skills>listSkill = new ArrayList<>();
        listSkill.add(Skills.ANGULAR);
        listSkill.add(Skills.GCP);
        listSkill.add(Skills.DELIVERY);
        listSkill.add(Skills.JAVA);
        listSkill.add(Skills.SELENIUM);
        listSkill.add(Skills.REACT);
        listSkill.add(Skills.JENKINS);
        listSkill.add(Skills.PHP);
        return listSkill;
    }

}