package com.cgi.topic1;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;

public class Manager extends Personne {
    private ArrayList<Personne> listeMenbre = new ArrayList<Personne>();

    public Manager(Genre sexe, String nom, String prenom, String anniversaire, int taille) throws ParseException {
        super(sexe, nom, prenom, anniversaire, taille);

    }

    public Manager() {

    }

    public ArrayList<Personne> getListeMenbre() {
        return listeMenbre;
    }

    public void setListeMenbre(ArrayList<Personne> listeMenbre) {
        this.listeMenbre = listeMenbre;
    }

    public void addMenber(Develloper dev) throws ParseException {
        this.listeMenbre.add(dev);
    }

    public void displayListMenber(){
        for (Personne p:this.listeMenbre
        ) {
            System.out.println("la liste des noms de ce manager est : "+p.getNom()+" "+p.getAnniversaire());
        }
    }

}