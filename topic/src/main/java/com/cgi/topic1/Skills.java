package com.cgi.topic1;

public enum Skills {
    JAVA,
    PHP,
    ANGULAR,
    DELIVERY,
    SELENIUM,
    GCP,
    REACT,
    JENKINS
}
