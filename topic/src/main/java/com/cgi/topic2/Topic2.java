package com.cgi.topic2;

import com.cgi.App;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;


    public class Topic2 {
        public static void main2() throws TraitementException, ParseException {
            /*Initialisations des variables*/
            int i = 0;
            String nombre;
            String chiffre;
            boolean flag = false;
            boolean doWhile = false;
            ArrayList<Integer> listNbre = new ArrayList<Integer>();

            /*instanciation de l Objet permettant de recuperer les données */
            Scanner scanner = new Scanner(System.in);
/*
boucle do while pour gerer les saisies de l'utilisateur
 */
            do {
                /*Recuperation de la donnée du nombre d'élémnts*/
                System.out.println("Veuillez saisir le nombre d'éléments :");
                String elements = scanner.next();

                /*verification si l'element est valide -> gestion des exceptions*/
                while (gestionExceptionsNombreElements(elements) == false) {
                    elements = scanner.next();
                }
                /*Saisie et verif des nombres données*/
                while (i < Integer.parseInt(elements)) {
                    System.out.print("Veuillez saisir un nombre : ");
                    nombre = scanner.next();
                    while (gestionExceptions(nombre) == false) {
                        nombre = scanner.next();
                    }
                    listNbre.add(Integer.parseInt(nombre));
                    i++;
                }
                i=0;
                /*saisie du nombre à rechercher*/
                System.out.println("Veuillez saisir le chiffre à retrouver : ");
                chiffre = scanner.next();
                while (gestionExceptions(chiffre) == false) {
                    chiffre = scanner.next();
                }


                try {
                    /*recherche du nombre a trouver et gestion des conditions */
                    for (int j = 0; j < listNbre.size(); j++) {
                        if(listNbre.get(j)==Integer.parseInt(chiffre)){
                            System.out.println("Votre nombre dans l'arrayList est à l'index : " + j);
                            flag=true;
                            break;
                        }}
                }catch (Exception e) {
                    System.out.println("Le numero n est pas trouvé.");
                }
                if(flag==false){
                    System.out.println("Le numero n est pas trouvé.");
                }
                Collections.sort(listNbre);
                System.out.println("Pour le plaisir je vous ai trie les nombres :"+listNbre.toString());
                System.out.println("Voulez vous continuer si oui tapez 'O' si non tapez 'N' :");
                String choice = scanner.next();
                while(!("o".equalsIgnoreCase(choice)||choice.equalsIgnoreCase("n"))){
                    System.out.println("Mauvaise saisie, voulez vous continuer si oui tapez 'O' si non tapez 'N' :");
                    choice = scanner.next();
                }
                if(choice.equalsIgnoreCase("n")){
                    doWhile=true;
                    App main = new App();
                    String [] args = null;
                    main.main(args);

                }
                listNbre.clear();

            }while (doWhile==false);
        }
        /*
        methode de gestion des exceptions sur le nombre d'elements à saisir
                un paramatre string à rentrer
         */
        public static boolean gestionExceptions(String number) {
            boolean reponse = false;
            try {
                if (!number.isEmpty()) {
                    int ok = Integer.parseInt(number);
                    reponse = true;
                    return reponse;
                }
            } catch (Exception traitementException) {
                System.out.println("Votre saisie est incorrecte veuillez recommencer :");
                return reponse;
            }

            return reponse;
        }
        /*
        methode de gestion des exceptions sur le nombre d'elements à saisir
        un paramatre string à rentrer
         */
        public static boolean gestionExceptionsNombreElements(String number) {
            boolean reponse = false;
            try {
                if(Integer.parseInt(number)<=0){
                    System.out.println("Votre saisie est incorrecte, le nombre d'élement doit être supérieur à 0, veuillez recommencer :");
                    return reponse;
                }
                else if (!number.isEmpty()) {
                    int ok = Integer.parseInt(number);
                    reponse = true;
                    return reponse;
                }
            } catch (Exception traitementException) {
                System.out.println("Votre saisie est incorrecte veuillez recommencer :");
                return reponse;
            }

            return reponse;
        }


    }


