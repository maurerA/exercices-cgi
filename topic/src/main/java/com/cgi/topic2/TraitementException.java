package com.cgi.topic2;

public class TraitementException extends Exception{
    public TraitementException(String s) {
        super(s);
    }
}
